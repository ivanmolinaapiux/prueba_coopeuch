# test-rest-coopeuch

Test API Rest Spring Boot con Swagger, acceso a BD Postgres 12 llamada "COOPEUCHPRUEBA" y test unitario dummy para cumplir con la formalidad de incluir test unitarios.

## Para probar el código:

1. Tener disponible una base de datos Postgresql 12 llamada "COOPEUCHPRUEBA" (sin comillas)
2. Acciones 
  - a) Si solo se desea compilar el war, ejecutar: mvn clean install
  - b) Si se desea correr y hacer deploy, ejecutar: mvn spring-boot:run
3. Para probar los endpoints se usó la extension RESTED de Firefox
  - a) Para el endpoint de insertar/modificar usar este json de plantilla con metodo POST
     hacia la URL http://localhost:8080/coopeuchprueba/tarea con header
     conten-type=application/json y body con los siguiente:
    - *=================================*
    - {
       "identificador": null,
       "descripcion": "<Ingresa la descripcion>",
       "fechaCreacion": "yyyy-MM-ddTHH:mm:ss.SSS",
       "vigente": true/false/null (este ultimo lo interpreta como false)
      }

     *=================================*  
    - indicando un identificador se hace el update, dejando en nulo o 0 se hace insertar/modificar
      ejemplos:

    - *=============insertar====================*
    -   {
         "identificador": null,
         "descripcion": "Tarea Uno",
         "fechaCreacion": "2020-10-31T21:45:59.133",
         "vigente": true
        }

    - *=============fin insertar====================*  

    - *=============modificar====================*
    - {
       "identificador": 1,
       "descripcion": "Tarea Nro. Uno",
       "fechaCreacion": "2021-12-31T14:32:36.166",
       "vigente": false
      }

     - *=============fin insertar====================*  

  - b) Para el endpoint de listar estan los endpoint GET
       - http://localhost:8080/coopeuchprueba/tarea (todas)
       - http://localhost:8080/coopeuchprueba/tarea/{identificador} que arroja una tarea en particular, ejm:
       http://localhost:8080/coopeuchprueba/1
   - c) El metodo de borrar es lo mismo que el de GET con identificador
        salvo que el verbo es DELETE
 
